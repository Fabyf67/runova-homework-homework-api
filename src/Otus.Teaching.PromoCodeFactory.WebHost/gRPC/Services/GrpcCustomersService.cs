﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using gRPCustomers;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.gRPC.Services
{

    public class GrpcCustomerService : GrpcCustomers.GrpcCustomersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public GrpcCustomerService(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GrpcListCustomerShortResponse> GetCustomersAsync(Empty request, ServerCallContext context)
        {
            var customerShortResponseListGrpc = new GrpcListCustomerShortResponse();
            var customers = await _customerRepository.GetAllAsync();
            foreach (var customer in customers)
            {
                customerShortResponseListGrpc.CustomerShortResponses.Add(new GrpcCustomerShortResponse()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                });
            }

            return customerShortResponseListGrpc;
        }

        public override async Task<GrpcCustomerResponse> GetCustomerAsync(CustomerId request, ServerCallContext context)
        {
            GrpcCustomerResponse CustomerResponseGrpc = new GrpcCustomerResponse();
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                return null;
            else
            {
                var customerGrpc = new GrpcCustomerResponse()
                {
                    Id = customer.Id.ToString(),
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName
                };
                if (customer.Preferences.Any())
                    customerGrpc.Preferences.AddRange(customer.Preferences.Select(x => new GrpcPreferenceResponse()
                    {
                        Id = x.PreferenceId.ToString(),
                        Name = x.Preference.Name
                    }));


                return customerGrpc;
            }
        }

        public override async Task<GrpcCustomerResponse> CreateCustomerAsync(GrpcCreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x)).ToList());

            var newCustomerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = newCustomerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = preferences.Select(x => new CustomerPreference()
                {
                    CustomerId = newCustomerId,
                    Preference = x,
                    PreferenceId = x.Id
                }).ToList()
            };

            await _customerRepository.AddAsync(customer);

            return new GrpcCustomerResponse() { Id = customer.Id.ToString() };

        }

        public override async Task<GrpcCustomerResponse> EditCustomerAsync(GrpcCreateOrEditCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id.ToString()));

            if (customer == null)
                throw new ArgumentNullException($"Customer with ID: {request.Id.ToString()} found");

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x)).ToList());

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = customer.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();


            await _customerRepository.UpdateAsync(customer);

            var gRPCCustomer = await GetCustomerAsync(new CustomerId() { Id = request.Id.ToString() }, context);

            return gRPCCustomer;
        }

        public override async Task<Empty> DeleteCustomerAsync(CustomerId request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id));

            if (customer == null)
                throw new ArgumentException($"Customer with ID: {request.Id.ToString()} found");

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }

}
